
// Akkuaufnahme für Bosch Professional 18V

// Zugehörige Skizze in 
// team14-lemming2/3d/batterie-adapter/Bosch/skizze.jpg
a	=	60.4;
b	=	6.5	;
c	=	52.4;
k	=	11.3; // y+
m	=	5	; // y-
r	=	1	; // y-


A   =   30  ;
B   =   16  ;
// C siehe unten
D	=	16	;
E	=	21	;
F	=	39.7;
G	=	7	; // z-
H	=	70	; // +/-x
I	=	40	;
J	=	6	;
L	=	56	;
N	=	22	;
P	=	11	;
Q	=	8.2	;
S	=	58	;
T	=	60	; // z+
U	=	0.8	;

C   =   N/2+J+3+4.2/2;
echo(C);

eps = 1; // Epsilon, um z-Fight zu verhindern
eps2 = 2*eps;
ome = 200; // große dimension über alles


difference(){
    // Hauptvolumen
    translate([-H/2, -(r+m), -G])
        cube([H, k+r+m, T+G]);
 
    union(){
        // Akkueinschub
        translate([-c/2, eps, 0])
            cube([c, k+eps, S+eps]);

        // Flachstelle bei der Arretierung
        translate([-H/2-eps, 0, S])
            cube([H+eps2, k+eps, T-S+eps]);
        
        // Nut
        translate([-a/2, 0, 0])
            cube([a, b, S+eps]);
        
        // Kontaktfahnen
        translate([-I/2, 0, J])
            cube([U, ome, Q], center=true);
        translate([I/2, 0, J])
            cube([U, ome, Q], center=true);
        
        // Loch für Arretierung
        translate([-E/2, -ome/2, F])
            cube([E, ome, D]);
            
        // Rückentasche für Platinen
        translate([-L/2, -(m+eps+r), J-N/2])
            cube([L, m+eps, N]);
            
        // Kabelauslass
        translate([-5, -(m+eps+r), -ome+J])
            cube([10, m+eps, ome]);
            
        translate([A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
    }
}

module schraube(){
    $fn = 64;
    union(){
        cylinder(h=10, d=8.4);
        translate([0,0, -2.1+0.05]) 
            cylinder(h=2.1, d1=4.2, d2=8.4);
        translate([0,0, -12.1+0.1]) 
            cylinder(h=10, d=4.2);
    }
}

//schraube();
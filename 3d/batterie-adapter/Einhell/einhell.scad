
// Akkuaufnahme für Bosch Professional 18V

// Zugehörige Skizze in 
// team14-lemming2/3d/batterie-adapter/Bosch/skizze.jpg

// Changelog
// v2: b von 3.2 auf 3.4mm vergrößert, v1 passte, aber sehr streng

a	=	48.3	;
b	=	3.4	;
c	=	42.8	;
k	=	7.7	;
m	=	5.0	;
r	=	1.0	;

A	=	25;
B	=	35;
C	=	5;
D	=	8.4	;
E	=	32.0	;
F	=	46.0	;
G	=	8.0	;
H	=	62.0	;
I	=	24.2	;
J	=	23.5	;
L	=	52.2	;
N	=	22.0	;
P	=	11.0	;
Q	=	8.2	;
S	=	43.0	;
T	=	56	;
U	=	0.8	;


eps = 1; // Epsilon, um z-Fight zu verhindern
eps2 = 2*eps;
ome = 200; // große dimension über alles


difference(){
    // Hauptvolumen
    translate([-H/2, -(r+m), -G])
        cube([H, k+r+m, T+G]);
 
    union(){
        // Akkueinschub
        translate([-c/2, eps, 0])
            cube([c, k+eps, S+eps]);

        // Flachstelle bei der Arretierung
        translate([-H/2-eps, 0, S])
            cube([H+eps2, k+eps, T-S+eps]);
        
        // Nut
        translate([-a/2, 0, 0])
            cube([a, b, S+eps]);
        
        // Kontaktfahnen
        translate([-I/2, 0, J])
            cube([U, ome, Q], center=true);
        translate([I/2, 0, J])
            cube([U, ome, Q], center=true);
        
        // Loch für Arretierung
        translate([-E/2, -ome/2, F])
            cube([E, ome, D]);
            
        // Rückentasche für Platinen
        translate([-L/2, -(m+eps+r), J-N/2])
            cube([L, m+eps, N]);
            
        // Kabelauslass
        translate([-5, -(m+eps+r), -ome+J])
            cube([10, m+eps, ome]);
            
        translate([A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
            
        translate([0,k-1,-6]) 
            rotate([90,0,180]) 
            linear_extrude(2) 
            text("EINHELL v2", 5, halign="center");
    }
}

module schraube(){
    $fn = 64;
    union(){
        cylinder(h=10, d=8.4);
        translate([0,0, -2.1+0.05]) 
            cylinder(h=2.1, d1=4.2, d2=8.4);
        translate([0,0, -12.1+0.1]) 
            cylinder(h=10, d=4.2);
    }
}

//schraube();
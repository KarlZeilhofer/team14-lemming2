
// Akkuaufnahme für Makita 18V

// Zugehörige Skizze in 
// team14-lemming2/3d/batterie-adapter/Bosch/skizze.jpg

a	=	61.0	;
b	=	9.0	;
c	=	54.0	;
k	=	14.5	; // y+
m	=	5.0	; // y-
r	=	1.0	; // y-


A   =   28  ;
B   =   36  ;
C   =   6;
D	=	13.0	;

E	=	36.0	;
F	=	50.5	;
G	=	 8.0    ; // z-
H	=	71.0	; // +/-x
I	=	39.0	;
J	=	23.0	;
L	=	55.0	;
N	=	22.0	;
P	=	11.0	;
Q	=	8.2	;
S	=	43.5	;
T	=	68.0	; // z+
U	=	0.8	;



eps = 1;
eps2 = 2*eps;
ome = 200; // große dimension über alles


difference(){
    // Hauptvolumen
    translate([-H/2, -(r+m), -G])
        cube([H, k+r+m, T+G]);
 
    union(){
        // Akkueinschub
        translate([-c/2, eps, 0])
            cube([c, k+eps, S+eps]);

        // Flachstelle bei der Arretierung
        translate([-H/2-eps, 0, S])
            cube([H+eps2, k+eps, T-S+eps]);
        
        // Nut
        translate([-a/2, 0, 0])
            cube([a, b, S+eps]);
        
        // Kontaktfahnen
        translate([-I/2, 0, J])
            cube([U, ome, Q], center=true);
        translate([I/2, 0, J])
            cube([U, ome, Q], center=true);
        
        // Loch für Arretierung
        translate([-E/2, -ome/2, F])
            cube([E, ome, D]);
            
        // Rückentasche für Platinen
        translate([-L/2, -(m+eps+r), J-N/2])
            cube([L, m+eps, N]);
            
        // Kabelauslass
        translate([-5, -(m+eps+r), -ome+J])
            cube([10, m+eps, ome]);
        
            
        // Schrauben
        translate([A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
            
        // 1. Sondergeometrie Makita: 
        // Zusatzausnehmung bei Arretierungsloch
        translate([-ome/2, -1.6, 59])
            cube([ome, ome, ome]);
            
        // 2. Sondergeometrie Makita
        // Ausnehmung beim hinteren Anschlag
        translate([-c/2, k-2.5, -ome+eps])
            cube([c, ome, ome]);
    }
}

module schraube(){
    $fn = 64;
    union(){
        cylinder(h=10, d=8.4);
        translate([0,0, -2.1+0.05]) 
            cylinder(h=2.1, d1=4.2, d2=8.4);
        translate([0,0, -12.1+0.1]) 
            cylinder(h=10, d=4.2);
    }
}

//schraube();
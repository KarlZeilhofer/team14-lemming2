
// Akkuaufnahme für Metabo M Ultra, 18V

// Zugehörige Skizze in 
// team14-lemming2/3d/batterie-adapter/
a	=	58.2;
b	=	3.8;
c	=	47.3;
k	=	7.5; // y+
m	=	5	; // y-
r	=	1	; // y-


A   =   28  ;
B   =   45  ;
C   =   -6  ;
//D	=	16	;
//E	=	21	;
//F	=	39.7;
G	=	12	; // z-
H	=	77	; // +/-x
I	=	34.4	;
J	=	20	;
L	=	51	;
N	=	22	;
P	=	11	;
Q	=	8.2	;
S	=	48.5	;
T	=	60	; // z+
U	=	0.8	;

eps = 1; // Epsilon, um z-Fight zu verhindern
eps2 = 2*eps;
ome = 200; // große dimension über alles


difference(){
    // Hauptvolumen
    translate([-H/2, -(r+m), -G])
        cube([H, k+r+m, T+G]);
 
    union(){
        // Akkueinschub
        translate([-c/2, eps, 0])
            cube([c, k+eps, S+eps]);

        // Flachstelle bei der Arretierung
        translate([-H/2-eps, 0, S])
            cube([H+eps2, k+eps, T-S+eps]);
        
        // Nut
        translate([-a/2, 0, 0])
            cube([a, b, S+eps]);
        
        // Kontaktfahnen
        translate([-I/2, 0, J])
            cube([U, ome, Q], center=true);
        translate([I/2, 0, J])
            cube([U, ome, Q], center=true);
        
        // Loch für Arretierung
        // Metabo hat dieses Loch nicht!
        //translate([-E/2, -ome/2, F])
        //    cube([E, ome, D]);
            
        // Rückentasche für Platinen
        translate([-L/2, -(m+eps+r), J-N/2])
            cube([L, m+eps, N]);
            
        // Kabelauslass
        translate([-5, -(m+eps+r), -ome+J])
            cube([10, m+eps, ome]);
            
        translate([A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
            
        translate([0,-1,T-7]) 
            rotate([90,0,180]) 
            linear_extrude(2) 
            text("METABO v1", 5, halign="center");
    }
}

module schraube(){
    $fn = 64;
    union(){
        cylinder(h=10, d=8.4);
        translate([0,0, -2.1+0.05]) 
            cylinder(h=2.1, d1=4.2, d2=8.4);
        translate([0,0, -12.1+0.1]) 
            cylinder(h=10, d=4.2);
    }
}

//schraube();
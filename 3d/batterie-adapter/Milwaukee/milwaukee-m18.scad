
// Akkuaufnahme für Bosch Professional 18V

// Zugehörige Skizze in 
// team14-lemming2/3d/batterie-adapter/Bosch/skizze.jpg
a	=	46.5;
b	=	7.4	;
c	=	38.0;
k	=	13.5; // y+
m	=	5	; // y-
r	=	1	; // y-


A   =   22  ;
B   =   35  ;
C   =   1;
D	=	11.5	;
E	=	54	;
// F	=	S; // siehe unten
G	=	8	; // z-
H	=	75	; // +/-x
I	=	22.5	;
J	=	16.5	;
L	=	50.5	;
N	=	22	;
P	=	11	;
Q	=	8.2	;
S	=	44.5;
T	=	60	; // z+
U	=	0.8	;

F = S;


eps = 1; // Epsilon, um z-Fight zu verhindern
eps2 = 2*eps;
ome = 200; // große dimension über alles


difference(){
    // Hauptvolumen
    translate([-H/2, -(r+m), -G])
        cube([H, k+r+m, T+G]);
 
    union(){
        // Akkueinschub
        translate([-c/2, eps, 0])
            cube([c, k+eps, S+eps]);

        // Flachstelle bei der Arretierung
        // Milwaukee-Spezifisch
        translate([-a/2, 0, S])
            cube([a, k+eps, T-S+eps]);
        
        // Loch für Arretierung
        // Milwaukee Spezial
        translate([-E/2, 0, F])
            cube([E, ome, D]);

        // Nut
        translate([-a/2, 0, 0])
            cube([a, b, S+eps]);
        
        // Kontaktfahnen
        translate([-I/2, 0, J])
            cube([U, ome, Q], center=true);
        translate([I/2, 0, J])
            cube([U, ome, Q], center=true);
        
            
        // Rückentasche für Platinen
        translate([-L/2, -(m+eps+r), J-N/2])
            cube([L, m+eps, N]);
            
        // Kabelauslass
        translate([-5, -(m+eps+r), -ome+J])
            cube([10, m+eps, ome]);
            
        // Milwaukee Sondergeometrie:
        // Nut bei den Batteriekontakten
        translate([-a/2+12.4, 11.4, -ome+eps])
            cube([5, ome, ome]);
            
        translate([A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C]) 
            rotate([-90,0,0]) 
            schraube();
        translate([-A/2, -eps, C+B]) 
            rotate([-90,0,0]) 
            schraube();
            
        translate([0,-1,T-7]) 
            rotate([90,0,180]) 
            linear_extrude(2) 
            text("MILWAUKEE", 5, halign="center");
    }
}

module schraube(){
    $fn = 64;
    union(){
        cylinder(h=ome, d=8.4);
        translate([0,0, -2.1+0.05]) 
            cylinder(h=2.1, d1=4.2, d2=8.4);
        translate([0,0, -12.1+0.1]) 
            cylinder(h=10, d=4.2);
    }
}

//schraube();
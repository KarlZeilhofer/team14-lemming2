


xi=21.3;

yi=6.5;
zi=10;
w =0.86;
eps=0.1;

difference(){
    cube([xi+2*w, yi+2*w, zi+w]);
    translate([w, w, -eps]) 
        cube([xi, yi, zi]);
}
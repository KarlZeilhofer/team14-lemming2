EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title "Lemming Robo 2"
Date "2021-07-18"
Rev "21.0.1-dev"
Comp "Team14 GesbR"
Comment1 "by Karl Zeilhofer"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2050 1300 900  1350
U 60CBA5B1
F0 "USB" 50
F1 "usb.sch" 50
F2 "TxD" O R 2950 2400 50 
F3 "~DTR" O R 2950 1900 50 
F4 "~RTR" O R 2950 2000 50 
F5 "RxD" I R 2950 2500 50 
F6 "VUSB" O R 2950 1400 50 
F7 "5V" I L 2050 1400 50 
F8 "3V3" O R 2950 1500 50 
$EndSheet
$Sheet
S 3350 1700 800  450 
U 60CB776D
F0 "Autoreset" 50
F1 "autoreset.sch" 50
F2 "~RESET" O R 4150 1900 50 
F3 "GPIO0" O R 4150 2000 50 
F4 "~DTR" I L 3350 1900 50 
F5 "~RTS" I L 3350 2000 50 
F6 "3V3" I L 3350 1800 50 
$EndSheet
NoConn ~ 4550 4600
NoConn ~ 4550 4700
NoConn ~ 4550 4800
NoConn ~ 4550 4900
NoConn ~ 4550 5000
NoConn ~ 4550 5100
Text HLabel 10100 1500 2    50   Output ~ 0
3V3
Wire Wire Line
	2950 1500 3250 1500
Wire Wire Line
	2950 1900 3350 1900
Wire Wire Line
	2950 2000 3350 2000
Wire Wire Line
	5750 4000 5950 4000
Text HLabel 6750 4000 2    50   Output ~ 0
~MotorEnable
Text HLabel 6750 5200 2    50   Output ~ 0
L_Step
Text HLabel 6750 4200 2    50   Output ~ 0
R_Step
Text HLabel 6750 5100 2    50   Output ~ 0
L_Dir
Text HLabel 6750 5300 2    50   Output ~ 0
R_Dir
Text HLabel 7400 4500 2    50   Output ~ 0
MicroStep1
Text HLabel 7400 4600 2    50   Output ~ 0
MicroStep2
Text HLabel 7400 4700 2    50   Output ~ 0
MicroStep3
Text HLabel 6750 4900 2    50   BiDi ~ 0
SCL
Text HLabel 6750 4800 2    50   BiDi ~ 0
SDA
Wire Wire Line
	5750 5100 5950 5100
Wire Wire Line
	6750 5200 6250 5200
Wire Wire Line
	6750 4200 6250 4200
Wire Wire Line
	6750 5300 6250 5300
Wire Wire Line
	6750 4900 6250 4900
Wire Wire Line
	6750 4800 6250 4800
Text HLabel 6750 5500 2    50   Output ~ 0
Buzzer
Wire Wire Line
	6750 5500 6250 5500
Text HLabel 7400 3900 2    50   Output ~ 0
~MotorSleep
Text HLabel 1250 1400 0    50   Input ~ 0
5V
Wire Wire Line
	1250 1400 2050 1400
Text HLabel 7400 4400 2    50   Output ~ 0
~MotorReset
$Comp
L htl_modules:ESP32-WROOM-32 U2
U 1 1 60DEB951
P 5150 4600
F 0 "U2" H 4700 6000 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 5650 3250 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 5150 3100 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 4850 4650 50  0001 C CNN
F 4 "LCSC" H 5150 4600 50  0001 C CNN "Distributor"
F 5 "C82899" H 5150 4600 50  0001 C CNN "DistOrderNr"
	1    5150 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 60DED7DC
P 5100 6200
F 0 "#PWR010" H 5100 5950 50  0001 C CNN
F 1 "GND" H 5105 6027 50  0000 C CNN
F 2 "" H 5100 6200 50  0001 C CNN
F 3 "" H 5100 6200 50  0001 C CNN
	1    5100 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 6200 5100 6100
Wire Wire Line
	5100 6100 5050 6100
Wire Wire Line
	4950 6100 4950 6000
Wire Wire Line
	5050 6000 5050 6100
Connection ~ 5050 6100
Wire Wire Line
	5050 6100 4950 6100
Wire Wire Line
	5100 6100 5150 6100
Wire Wire Line
	5150 6100 5150 6000
Connection ~ 5100 6100
Wire Wire Line
	5150 6100 5250 6100
Wire Wire Line
	5250 6100 5250 6000
Connection ~ 5150 6100
Wire Wire Line
	5150 1500 5150 3200
Connection ~ 5150 1500
Wire Wire Line
	5150 1500 10100 1500
Text HLabel 6750 3800 2    50   Input ~ 0
ADC_Bat
Wire Wire Line
	6750 3800 6250 3800
Wire Wire Line
	4150 2000 6900 2000
Wire Wire Line
	6900 2000 6900 3400
Wire Wire Line
	6900 3400 6250 3400
Wire Wire Line
	4150 1900 4400 1900
Wire Wire Line
	4400 1900 4400 2900
Wire Wire Line
	4400 3400 4550 3400
Wire Wire Line
	2950 2400 6750 2400
Wire Wire Line
	6750 2400 6750 3700
Wire Wire Line
	6750 3700 6250 3700
Wire Wire Line
	2950 2500 6700 2500
Wire Wire Line
	6700 2500 6700 3500
Wire Wire Line
	6700 3500 6250 3500
Wire Wire Line
	5750 3900 5950 3900
Wire Wire Line
	7400 4400 6250 4400
Wire Wire Line
	7400 4500 6250 4500
Wire Wire Line
	5750 4600 5950 4600
Wire Wire Line
	7400 4700 6250 4700
$Comp
L Device:R J3
U 1 1 60DFAE72
P 6100 3400
F 0 "J3" V 6100 3400 50  0000 C CNN
F 1 "-" V 6000 3650 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 3400 50  0001 C CNN
F 3 "~" H 6100 3400 50  0001 C CNN
F 4 "virtual" H 6100 3400 50  0001 C CNN "DistOrderNr"
	1    6100 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 3400 5750 3400
$Comp
L Device:R J4
U 1 1 60DFB473
P 6100 3500
F 0 "J4" V 6100 3500 50  0000 C CNN
F 1 "-" V 5984 3500 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 3500 50  0001 C CNN
F 3 "~" H 6100 3500 50  0001 C CNN
F 4 "virtual" H 6100 3500 50  0001 C CNN "DistOrderNr"
	1    6100 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 3500 5750 3500
$Comp
L Device:R J5
U 1 1 60DFB67E
P 6100 3600
F 0 "J5" V 6100 3600 50  0000 C CNN
F 1 "-" V 5984 3600 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 3600 50  0001 C CNN
F 3 "~" H 6100 3600 50  0001 C CNN
F 4 "virtual" H 6100 3600 50  0001 C CNN "DistOrderNr"
	1    6100 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R J6
U 1 1 60DFB92B
P 6100 3700
F 0 "J6" V 6100 3700 50  0000 C CNN
F 1 "-" V 5984 3700 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 3700 50  0001 C CNN
F 3 "~" H 6100 3700 50  0001 C CNN
F 4 "virtual" H 6100 3700 50  0001 C CNN "DistOrderNr"
	1    6100 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 3700 5750 3700
$Comp
L Device:R J7
U 1 1 60DFEBCC
P 6100 3800
F 0 "J7" V 6100 3800 50  0000 C CNN
F 1 "-" V 5984 3800 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 3800 50  0001 C CNN
F 3 "~" H 6100 3800 50  0001 C CNN
F 4 "virtual" H 6100 3800 50  0001 C CNN "DistOrderNr"
	1    6100 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 3800 5750 3800
$Comp
L Device:R J8
U 1 1 60DFEEAD
P 6100 3900
F 0 "J8" V 6100 3900 50  0000 C CNN
F 1 "-" V 5984 3900 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 3900 50  0001 C CNN
F 3 "~" H 6100 3900 50  0001 C CNN
F 4 "virtual" H 6100 3900 50  0001 C CNN "DistOrderNr"
	1    6100 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 3900 7400 3900
$Comp
L Device:R J9
U 1 1 60DFF10F
P 6100 4000
F 0 "J9" V 6100 4000 50  0000 C CNN
F 1 "-" V 5984 4000 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4000 50  0001 C CNN
F 3 "~" H 6100 4000 50  0001 C CNN
F 4 "virtual" H 6100 4000 50  0001 C CNN "DistOrderNr"
	1    6100 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 4000 6750 4000
$Comp
L Device:R J10
U 1 1 60DFF38D
P 6100 4100
F 0 "J10" V 6100 4100 50  0000 C CNN
F 1 "-" V 5984 4100 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4100 50  0001 C CNN
F 3 "~" H 6100 4100 50  0001 C CNN
F 4 "virtual" H 6100 4100 50  0001 C CNN "DistOrderNr"
	1    6100 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R J11
U 1 1 60DFF7DC
P 6100 4200
F 0 "J11" V 6100 4200 50  0000 C CNN
F 1 "-" V 5984 4200 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4200 50  0001 C CNN
F 3 "~" H 6100 4200 50  0001 C CNN
F 4 "virtual" H 6100 4200 50  0001 C CNN "DistOrderNr"
	1    6100 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 4200 5750 4200
$Comp
L Device:R J12
U 1 1 60DFF9DD
P 6100 4300
F 0 "J12" V 6100 4300 50  0000 C CNN
F 1 "-" V 5984 4300 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4300 50  0001 C CNN
F 3 "~" H 6100 4300 50  0001 C CNN
F 4 "virtual" H 6100 4300 50  0001 C CNN "DistOrderNr"
	1    6100 4300
	0    1    1    0   
$EndComp
$Comp
L Device:R J13
U 1 1 60DFFDBD
P 6100 4400
F 0 "J13" V 6100 4400 50  0000 C CNN
F 1 "-" V 5984 4400 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4400 50  0001 C CNN
F 3 "~" H 6100 4400 50  0001 C CNN
F 4 "virtual" H 6100 4400 50  0001 C CNN "DistOrderNr"
	1    6100 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 4400 5750 4400
$Comp
L Device:R J14
U 1 1 60DFFFB8
P 6100 4500
F 0 "J14" V 6100 4500 50  0000 C CNN
F 1 "-" V 5984 4500 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4500 50  0001 C CNN
F 3 "~" H 6100 4500 50  0001 C CNN
F 4 "virtual" H 6100 4500 50  0001 C CNN "DistOrderNr"
	1    6100 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 4500 5750 4500
$Comp
L Device:R J15
U 1 1 60E00135
P 6100 4600
F 0 "J15" V 6100 4600 50  0000 C CNN
F 1 "-" V 5984 4600 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4600 50  0001 C CNN
F 3 "~" H 6100 4600 50  0001 C CNN
F 4 "virtual" H 6100 4600 50  0001 C CNN "DistOrderNr"
	1    6100 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 4600 7400 4600
$Comp
L Device:R J16
U 1 1 60E002BE
P 6100 4700
F 0 "J16" V 6100 4700 50  0000 C CNN
F 1 "-" V 5984 4700 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4700 50  0001 C CNN
F 3 "~" H 6100 4700 50  0001 C CNN
F 4 "virtual" H 6100 4700 50  0001 C CNN "DistOrderNr"
	1    6100 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 4700 5750 4700
$Comp
L Device:R J17
U 1 1 60E004A6
P 6100 4800
F 0 "J17" V 6100 4800 50  0000 C CNN
F 1 "-" V 5984 4800 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4800 50  0001 C CNN
F 3 "~" H 6100 4800 50  0001 C CNN
F 4 "virtual" H 6100 4800 50  0001 C CNN "DistOrderNr"
	1    6100 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 4800 5750 4800
$Comp
L Device:R J18
U 1 1 60E00767
P 6100 4900
F 0 "J18" V 6100 4900 50  0000 C CNN
F 1 "-" V 5984 4900 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 4900 50  0001 C CNN
F 3 "~" H 6100 4900 50  0001 C CNN
F 4 "virtual" H 6100 4900 50  0001 C CNN "DistOrderNr"
	1    6100 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 4900 5750 4900
$Comp
L Device:R J19
U 1 1 60E00960
P 6100 5000
F 0 "J19" V 6100 5000 50  0000 C CNN
F 1 "-" V 5984 5000 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5000 50  0001 C CNN
F 3 "~" H 6100 5000 50  0001 C CNN
F 4 "virtual" H 6100 5000 50  0001 C CNN "DistOrderNr"
	1    6100 5000
	0    1    1    0   
$EndComp
$Comp
L Device:R J20
U 1 1 60E00B62
P 6100 5100
F 0 "J20" V 6100 5100 50  0000 C CNN
F 1 "-" V 5984 5100 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5100 50  0001 C CNN
F 3 "~" H 6100 5100 50  0001 C CNN
F 4 "virtual" H 6100 5100 50  0001 C CNN "DistOrderNr"
	1    6100 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	6250 5100 6750 5100
$Comp
L Device:R J21
U 1 1 60E00D1A
P 6100 5200
F 0 "J21" V 6100 5200 50  0000 C CNN
F 1 "-" V 5984 5200 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5200 50  0001 C CNN
F 3 "~" H 6100 5200 50  0001 C CNN
F 4 "virtual" H 6100 5200 50  0001 C CNN "DistOrderNr"
	1    6100 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 5200 5750 5200
$Comp
L Device:R J22
U 1 1 60E00FD6
P 6100 5300
F 0 "J22" V 6100 5300 50  0000 C CNN
F 1 "-" V 5984 5300 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5300 50  0001 C CNN
F 3 "~" H 6100 5300 50  0001 C CNN
F 4 "virtual" H 6100 5300 50  0001 C CNN "DistOrderNr"
	1    6100 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 5300 5750 5300
$Comp
L Device:R J23
U 1 1 60E011E0
P 6100 5400
F 0 "J23" V 6100 5400 50  0000 C CNN
F 1 "-" V 5984 5400 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5400 50  0001 C CNN
F 3 "~" H 6100 5400 50  0001 C CNN
F 4 "virtual" H 6100 5400 50  0001 C CNN "DistOrderNr"
	1    6100 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R J24
U 1 1 60E013D7
P 6100 5500
F 0 "J24" V 6100 5500 50  0000 C CNN
F 1 "-" V 5984 5500 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5500 50  0001 C CNN
F 3 "~" H 6100 5500 50  0001 C CNN
F 4 "virtual" H 6100 5500 50  0001 C CNN "DistOrderNr"
	1    6100 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 5500 5750 5500
$Comp
L Device:R J25
U 1 1 60E0166A
P 6100 5600
F 0 "J25" V 6100 5600 50  0000 C CNN
F 1 "-" V 5984 5600 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5600 50  0001 C CNN
F 3 "~" H 6100 5600 50  0001 C CNN
F 4 "virtual" H 6100 5600 50  0001 C CNN "DistOrderNr"
	1    6100 5600
	0    1    1    0   
$EndComp
$Comp
L Device:R J26
U 1 1 60E01C10
P 6100 5700
F 0 "J26" V 6100 5700 50  0000 C CNN
F 1 "-" V 5984 5700 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 6030 5700 50  0001 C CNN
F 3 "~" H 6100 5700 50  0001 C CNN
F 4 "virtual" H 6100 5700 50  0001 C CNN "DistOrderNr"
	1    6100 5700
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 60E01F07
P 4400 3150
F 0 "R8" H 4331 3196 50  0000 R CNN
F 1 "100R" H 4331 3105 50  0000 R CNN
F 2 "t14_standardSMD:R2012m" V 4330 3150 50  0001 C CNN
F 3 "~" H 4400 3150 50  0001 C CNN
F 4 "C115424" H 4400 3150 50  0001 C CNN "DistOrderNr"
F 5 "LCSC" H 4400 3150 50  0001 C CNN "Distributor"
	1    4400 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3300 4400 3400
$Comp
L Device:R J1
U 1 1 60E0267B
P 4050 3600
F 0 "J1" V 4050 3600 50  0000 C CNN
F 1 "-" V 3950 3600 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 3980 3600 50  0001 C CNN
F 3 "~" H 4050 3600 50  0001 C CNN
F 4 "virtual" H 4050 3600 50  0001 C CNN "DistOrderNr"
	1    4050 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R J2
U 1 1 60E02C41
P 4050 3700
F 0 "J2" V 4050 3700 50  0000 C CNN
F 1 "-" V 3934 3700 50  0001 C CNN
F 2 "t14_mech:Jumper_Normally_Closed_SMD3216m" V 3980 3700 50  0001 C CNN
F 3 "~" H 4050 3700 50  0001 C CNN
F 4 "virtual" H 4050 3700 50  0001 C CNN "DistOrderNr"
	1    4050 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 3600 4550 3600
Wire Wire Line
	4200 3700 4550 3700
Wire Wire Line
	5950 3600 5750 3600
Wire Wire Line
	5950 4100 5750 4100
Wire Wire Line
	5750 4300 5950 4300
Wire Wire Line
	5950 5000 5750 5000
Wire Wire Line
	5750 5400 5950 5400
Wire Wire Line
	5950 5600 5750 5600
Wire Wire Line
	5750 5700 5950 5700
Text HLabel 8450 5000 2    50   Input ~ 0
~Taster
Text HLabel 8450 4300 2    50   Output ~ 0
Selbsthaltung
Wire Wire Line
	6250 3600 7400 3600
Text HLabel 7400 3600 2    50   Output ~ 0
R_Trig
Text HLabel 7400 4100 2    50   Output ~ 0
L_Trig
Wire Wire Line
	6250 4100 7400 4100
Text HLabel 7400 5600 2    50   Input ~ 0
R_Echo
Text HLabel 7400 5700 2    50   Input ~ 0
L_Echo
Wire Wire Line
	7400 5600 6250 5600
Wire Wire Line
	6250 5700 7400 5700
Wire Wire Line
	8450 4300 6250 4300
Wire Wire Line
	8450 5000 6250 5000
Wire Wire Line
	3350 1800 3250 1800
Wire Wire Line
	3250 1800 3250 1500
Connection ~ 3250 1500
Wire Wire Line
	3250 1500 5150 1500
Wire Wire Line
	3900 3600 3750 3600
Wire Wire Line
	3750 3600 3750 3700
Wire Wire Line
	3900 3700 3750 3700
Connection ~ 3750 3700
Wire Wire Line
	3750 3700 3750 3850
$Comp
L power:GND #PWR09
U 1 1 60E8DBE7
P 3750 3850
F 0 "#PWR09" H 3750 3600 50  0001 C CNN
F 1 "GND" H 3755 3677 50  0000 C CNN
F 2 "" H 3750 3850 50  0001 C CNN
F 3 "" H 3750 3850 50  0001 C CNN
	1    3750 3850
	1    0    0    -1  
$EndComp
Text Notes 3550 3600 0    50   ~ 0
Unbenutzte Eingänge\nauf GND
Text Notes 4450 5200 1    50   ~ 0
Flash Schnittstelle\n(Nicht verwenden!)
$Comp
L power:GND #PWR011
U 1 1 60E8F28C
P 6400 5400
F 0 "#PWR011" H 6400 5150 50  0001 C CNN
F 1 "GND" H 6405 5227 50  0000 C CNN
F 2 "" H 6400 5400 50  0001 C CNN
F 3 "" H 6400 5400 50  0001 C CNN
	1    6400 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 5400 6400 5400
$Comp
L Device:C C?
U 1 1 60E99BAD
P 3050 3700
AR Path="/608DBC6E/60E99BAD" Ref="C?"  Part="1" 
AR Path="/608DBC3C/60930C19/60E99BAD" Ref="C?"  Part="1" 
AR Path="/60CB1E03/60CBA5B1/60CBBE2B/60E99BAD" Ref="C?"  Part="1" 
AR Path="/60CB1E03/60CBA5B1/60E99BAD" Ref="C?"  Part="1" 
AR Path="/60CB1E03/60E99BAD" Ref="C1"  Part="1" 
F 0 "C1" H 3165 3746 50  0000 L CNN
F 1 "1u" H 3165 3655 50  0000 L CNN
F 2 "t14_standardSMD:C2012m" H 3088 3550 50  0001 C CNN
F 3 "~" H 3050 3700 50  0001 C CNN
F 4 "C28323" H 3050 3700 50  0001 C CNN "DistOrderNr"
F 5 "LCSC" H 3050 3700 50  0001 C CNN "Distributor"
F 6 "CL21B105KBFNNNE" H 3050 3700 50  0001 C CNN "ManPartNr"
F 7 "Samsung" H 3050 3700 50  0001 C CNN "Manufacturer"
F 8 "1uF ±10% 50V X7R 0805 Multilayer Ceramic Capacitors MLCC - SMD/SMT RoHS" H 3050 3700 50  0001 C CNN "Notes"
F 9 "0.0113" H 3050 3700 50  0001 C CNN "PriceEUR"
F 10 "50" H 3050 3700 50  0001 C CNN "PriceForQty"
	1    3050 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60E99BB3
P 3050 3850
AR Path="/5F64F622/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/5F78AFC8/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/5F7F8851/5F7FB5B8/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/5F8070C3/5F7FB5B8/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/5F8AD0E2/5F7FB5B8/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/5FA85ABF/5F7FB5B8/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/5F8D661A/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/608DBC3C/60930C19/60930D3D/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/608DBC3C/60930C19/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/60CB1E03/60CBA5B1/60CBBE2B/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/60CB1E03/60CBA5B1/60E99BB3" Ref="#PWR?"  Part="1" 
AR Path="/60CB1E03/60E99BB3" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3050 3600 50  0001 C CNN
F 1 "GND" H 3055 3677 50  0000 C CNN
F 2 "" H 3050 3850 50  0001 C CNN
F 3 "" H 3050 3850 50  0001 C CNN
	1    3050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2900 3050 2900
Connection ~ 4400 2900
Wire Wire Line
	4400 2900 4400 3000
Wire Wire Line
	3050 2900 3050 3550
$EndSCHEMATC

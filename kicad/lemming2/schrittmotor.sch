EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title "Lemming Robo 2"
Date "2021-07-18"
Rev "21.0.1-dev"
Comp "Team14 GesbR"
Comment1 "by Karl Zeilhofer"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A?
U 1 1 60CB4EE5
P 3500 3100
AR Path="/60CB4EE5" Ref="A?"  Part="1" 
AR Path="/60CB434E/60CB4EE5" Ref="A4"  Part="1" 
F 0 "A4" H 3250 3750 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 4200 2300 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 3775 2350 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 3600 2800 50  0001 C CNN
F 4 "Amazon" H 3500 3100 50  0001 C CNN "Distributor"
	1    3500 3100
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A?
U 1 1 60CE4552
P 3500 5400
AR Path="/60CE4552" Ref="A?"  Part="1" 
AR Path="/60CB434E/60CE4552" Ref="A5"  Part="1" 
F 0 "A5" H 3250 6050 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 4200 4650 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 3775 4650 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 3600 5100 50  0001 C CNN
F 4 "Amazon" H 3500 5400 50  0001 C CNN "Distributor"
	1    3500 5400
	1    0    0    -1  
$EndComp
Text HLabel 1950 3000 0    50   Input ~ 0
~Enable
Text HLabel 1800 3100 0    50   Input ~ 0
L_Step
Text HLabel 1800 3200 0    50   Input ~ 0
L_Dir
Text HLabel 1850 5400 0    50   Input ~ 0
R_Step
Text HLabel 1850 5500 0    50   Input ~ 0
R_Dir
Text HLabel 1950 3400 0    50   Input ~ 0
MicroStep1
Text HLabel 1950 3500 0    50   Input ~ 0
MicroStep2
Text HLabel 1950 3600 0    50   Input ~ 0
MicroStep3
Text HLabel 1950 2700 0    50   Input ~ 0
~Reset
Text HLabel 1950 2800 0    50   Input ~ 0
~Sleep
Text HLabel 1950 2100 0    50   Input ~ 0
3V3
Wire Wire Line
	1950 2100 2300 2100
Wire Wire Line
	3500 2100 3500 2400
Wire Wire Line
	2300 2100 2300 4550
Wire Wire Line
	2300 4550 3500 4550
Wire Wire Line
	3500 4550 3500 4700
Connection ~ 2300 2100
Wire Wire Line
	2300 2100 3500 2100
$Comp
L power:GND #PWR022
U 1 1 60CE58B0
P 3600 4000
F 0 "#PWR022" H 3600 3750 50  0001 C CNN
F 1 "GND" H 3605 3827 50  0000 C CNN
F 2 "" H 3600 4000 50  0001 C CNN
F 3 "" H 3600 4000 50  0001 C CNN
	1    3600 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4000 3500 4000
Wire Wire Line
	3500 4000 3500 3900
Wire Wire Line
	3600 4000 3700 4000
Wire Wire Line
	3700 4000 3700 3900
Connection ~ 3600 4000
$Comp
L power:GND #PWR023
U 1 1 60CE6669
P 3600 6300
F 0 "#PWR023" H 3600 6050 50  0001 C CNN
F 1 "GND" H 3605 6127 50  0000 C CNN
F 2 "" H 3600 6300 50  0001 C CNN
F 3 "" H 3600 6300 50  0001 C CNN
	1    3600 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 6300 3500 6300
Wire Wire Line
	3500 6300 3500 6200
Wire Wire Line
	3600 6300 3700 6300
Wire Wire Line
	3700 6300 3700 6200
Connection ~ 3600 6300
Text HLabel 1950 1850 0    50   Input ~ 0
V_Batt
Wire Wire Line
	1950 1850 2350 1850
Wire Wire Line
	3700 1850 3700 2400
Wire Wire Line
	2350 1850 2350 4500
Wire Wire Line
	2350 4500 3700 4500
Wire Wire Line
	3700 4500 3700 4700
Connection ~ 2350 1850
Wire Wire Line
	2350 1850 3700 1850
Wire Wire Line
	1800 3100 3100 3100
Wire Wire Line
	3100 3200 1800 3200
Wire Wire Line
	1950 3400 2700 3400
Wire Wire Line
	1950 3500 2750 3500
Wire Wire Line
	1950 3600 2800 3600
Wire Wire Line
	1950 2700 2500 2700
Wire Wire Line
	1950 2800 2550 2800
Wire Wire Line
	1950 3000 2600 3000
Wire Wire Line
	2500 2700 2500 5000
Wire Wire Line
	2500 5000 3100 5000
Connection ~ 2500 2700
Wire Wire Line
	2500 2700 3100 2700
Wire Wire Line
	2550 2800 2550 5100
Wire Wire Line
	2550 5100 3100 5100
Connection ~ 2550 2800
Wire Wire Line
	2550 2800 3100 2800
Wire Wire Line
	2600 3000 2600 5300
Wire Wire Line
	2600 5300 3100 5300
Connection ~ 2600 3000
Wire Wire Line
	2600 3000 3100 3000
Wire Wire Line
	1850 5400 3100 5400
Wire Wire Line
	1850 5500 3100 5500
Wire Wire Line
	2700 3400 2700 5700
Wire Wire Line
	2700 5700 3100 5700
Connection ~ 2700 3400
Wire Wire Line
	2700 3400 3100 3400
Wire Wire Line
	2750 3500 2750 5800
Wire Wire Line
	2750 5800 3100 5800
Connection ~ 2750 3500
Wire Wire Line
	2750 3500 3100 3500
Wire Wire Line
	2800 3600 2800 5900
Wire Wire Line
	2800 5900 3100 5900
Connection ~ 2800 3600
Wire Wire Line
	2800 3600 3100 3600
Wire Wire Line
	4000 3000 4450 3000
Wire Wire Line
	4000 3300 4450 3300
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D69D54
P 4950 2850
AR Path="/60CF3D45/60D69D54" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D69D54" Ref="X2"  Part="1" 
F 0 "X2" H 5030 2800 50  0000 L CNN
F 1 "1B" H 5030 2891 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 2850 50  0001 C CNN
F 3 "~" H 4950 2850 50  0001 C CNN
F 4 "virtual" H 4950 2850 50  0001 C CNN "DistOrderNr"
	1    4950 2850
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D6AF01
P 4950 3050
AR Path="/60CF3D45/60D6AF01" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D6AF01" Ref="X3"  Part="1" 
F 0 "X3" H 5030 3000 50  0000 L CNN
F 1 "1A" H 5030 3091 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 3050 50  0001 C CNN
F 3 "~" H 4950 3050 50  0001 C CNN
F 4 "virtual" H 4950 3050 50  0001 C CNN "DistOrderNr"
	1    4950 3050
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D6BEBF
P 4950 3250
AR Path="/60CF3D45/60D6BEBF" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D6BEBF" Ref="X4"  Part="1" 
F 0 "X4" H 5030 3200 50  0000 L CNN
F 1 "2A" H 5030 3291 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 3250 50  0001 C CNN
F 3 "~" H 4950 3250 50  0001 C CNN
F 4 "virtual" H 4950 3250 50  0001 C CNN "DistOrderNr"
	1    4950 3250
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D6CEF0
P 4950 3450
AR Path="/60CF3D45/60D6CEF0" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D6CEF0" Ref="X5"  Part="1" 
F 0 "X5" H 5030 3400 50  0000 L CNN
F 1 "2B" H 5030 3491 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 3450 50  0001 C CNN
F 3 "~" H 4950 3450 50  0001 C CNN
F 4 "virtual" H 4950 3450 50  0001 C CNN "DistOrderNr"
	1    4950 3450
	1    0    0    1   
$EndComp
Wire Wire Line
	4450 3000 4450 2850
Wire Wire Line
	4450 2850 4750 2850
Wire Wire Line
	4600 3100 4600 3050
Wire Wire Line
	4600 3050 4750 3050
Wire Wire Line
	4000 3100 4600 3100
Wire Wire Line
	4600 3200 4600 3250
Wire Wire Line
	4600 3250 4750 3250
Wire Wire Line
	4000 3200 4600 3200
Wire Wire Line
	4450 3300 4450 3450
Wire Wire Line
	4450 3450 4750 3450
Wire Wire Line
	4000 5300 4450 5300
Wire Wire Line
	4000 5600 4450 5600
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D79351
P 4950 5150
AR Path="/60CF3D45/60D79351" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D79351" Ref="X6"  Part="1" 
F 0 "X6" H 5030 5100 50  0000 L CNN
F 1 "1B" H 5030 5191 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 5150 50  0001 C CNN
F 3 "~" H 4950 5150 50  0001 C CNN
F 4 "virtual" H 4950 5150 50  0001 C CNN "DistOrderNr"
	1    4950 5150
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D79357
P 4950 5350
AR Path="/60CF3D45/60D79357" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D79357" Ref="X7"  Part="1" 
F 0 "X7" H 5030 5300 50  0000 L CNN
F 1 "1A" H 5030 5391 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 5350 50  0001 C CNN
F 3 "~" H 4950 5350 50  0001 C CNN
F 4 "virtual" H 4950 5350 50  0001 C CNN "DistOrderNr"
	1    4950 5350
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D7935D
P 4950 5550
AR Path="/60CF3D45/60D7935D" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D7935D" Ref="X8"  Part="1" 
F 0 "X8" H 5030 5500 50  0000 L CNN
F 1 "2A" H 5030 5591 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 5550 50  0001 C CNN
F 3 "~" H 4950 5550 50  0001 C CNN
F 4 "virtual" H 4950 5550 50  0001 C CNN "DistOrderNr"
	1    4950 5550
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 X?
U 1 1 60D79363
P 4950 5750
AR Path="/60CF3D45/60D79363" Ref="X?"  Part="1" 
AR Path="/60CB434E/60D79363" Ref="X9"  Part="1" 
F 0 "X9" H 5030 5700 50  0000 L CNN
F 1 "2B" H 5030 5791 50  0000 L CNN
F 2 "t14_mech:Wirepad_5x3_SMT" H 4950 5750 50  0001 C CNN
F 3 "~" H 4950 5750 50  0001 C CNN
F 4 "virtual" H 4950 5750 50  0001 C CNN "DistOrderNr"
	1    4950 5750
	1    0    0    1   
$EndComp
Wire Wire Line
	4450 5300 4450 5150
Wire Wire Line
	4450 5150 4750 5150
Wire Wire Line
	4600 5400 4600 5350
Wire Wire Line
	4600 5350 4750 5350
Wire Wire Line
	4000 5400 4600 5400
Wire Wire Line
	4600 5500 4600 5550
Wire Wire Line
	4600 5550 4750 5550
Wire Wire Line
	4000 5500 4600 5500
Wire Wire Line
	4450 5600 4450 5750
Wire Wire Line
	4450 5750 4750 5750
$Comp
L Device:CP C?
U 1 1 60E293C8
P 3900 2050
AR Path="/608DBC83/60E293C8" Ref="C?"  Part="1" 
AR Path="/608DBC83/60A447A8/60E293C8" Ref="C?"  Part="1" 
AR Path="/60CF3D45/60A447A8/60E293C8" Ref="C?"  Part="1" 
AR Path="/60CB434E/60E293C8" Ref="C7"  Part="1" 
F 0 "C7" H 4018 2096 50  0000 L CNN
F 1 "100u/25V" H 4018 2005 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.9" H 3938 1900 50  0001 C CNN
F 3 "~" H 3900 2050 50  0001 C CNN
F 4 "C267469" H 3900 2050 50  0001 C CNN "DistOrderNr"
F 5 "LCSC" H 3900 2050 50  0001 C CNN "Distributor"
F 6 "RT1E101M0607" H 3900 2050 50  0001 C CNN "ManPartNr"
F 7 "ROQANG" H 3900 2050 50  0001 C CNN "Manufacturer"
F 8 "100uF ±20% 25V 6.3mm 7.7mm 2000hrs 105℃ -40℃~+105℃ SMD,6.3x7.7mm Aluminum Electrolytic Capacitors - SMD" H 3900 2050 50  0001 C CNN "Notes"
F 9 "0.039" H 3900 2050 50  0001 C CNN "PriceEUR"
F 10 "10" H 3900 2050 50  0001 C CNN "PriceForQty"
	1    3900 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1850 3900 1850
Wire Wire Line
	3900 1850 3900 1900
Wire Wire Line
	3900 2200 3900 2250
Connection ~ 3700 1850
$Comp
L power:GND #PWR024
U 1 1 60E2BFC3
P 3900 2250
F 0 "#PWR024" H 3900 2000 50  0001 C CNN
F 1 "GND" H 3905 2077 50  0000 C CNN
F 2 "" H 3900 2250 50  0001 C CNN
F 3 "" H 3900 2250 50  0001 C CNN
	1    3900 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 60E2C8D4
P 4200 4650
AR Path="/608DBC83/60E2C8D4" Ref="C?"  Part="1" 
AR Path="/608DBC83/60A447A8/60E2C8D4" Ref="C?"  Part="1" 
AR Path="/60CF3D45/60A447A8/60E2C8D4" Ref="C?"  Part="1" 
AR Path="/60CB434E/60E2C8D4" Ref="C8"  Part="1" 
F 0 "C8" H 4318 4696 50  0000 L CNN
F 1 "100u/25V" H 4318 4605 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.9" H 4238 4500 50  0001 C CNN
F 3 "~" H 4200 4650 50  0001 C CNN
F 4 "C267469" H 4200 4650 50  0001 C CNN "DistOrderNr"
F 5 "LCSC" H 4200 4650 50  0001 C CNN "Distributor"
F 6 "RT1E101M0607" H 4200 4650 50  0001 C CNN "ManPartNr"
F 7 "ROQANG" H 4200 4650 50  0001 C CNN "Manufacturer"
F 8 "100uF ±20% 25V 6.3mm 7.7mm 2000hrs 105℃ -40℃~+105℃ SMD,6.3x7.7mm Aluminum Electrolytic Capacitors - SMD" H 4200 4650 50  0001 C CNN "Notes"
F 9 "0.039" H 4200 4650 50  0001 C CNN "PriceEUR"
F 10 "10" H 4200 4650 50  0001 C CNN "PriceForQty"
	1    4200 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4500 4200 4500
Connection ~ 3700 4500
$Comp
L power:GND #PWR025
U 1 1 60E2F6FB
P 4200 4800
F 0 "#PWR025" H 4200 4550 50  0001 C CNN
F 1 "GND" H 4205 4627 50  0000 C CNN
F 2 "" H 4200 4800 50  0001 C CNN
F 3 "" H 4200 4800 50  0001 C CNN
	1    4200 4800
	1    0    0    -1  
$EndComp
$Comp
L htl_virtual:Item Item?
U 1 1 60EAC7FF
P 7400 3000
AR Path="/60EAC7FF" Ref="Item?"  Part="1" 
AR Path="/60CB434E/60EAC7FF" Ref="Item6"  Part="1" 
F 0 "Item6" H 7530 3046 50  0000 L CNN
F 1 "Header 8x2.54" H 7530 2955 50  0000 L CNN
F 2 "htl_virtual:Virtual-Item" H 7400 2930 50  0001 C CNN
F 3 "~" V 7400 3000 50  0001 C CNN
F 4 "LCSC" H 7400 3000 50  0001 C CNN "Distributor"
F 5 "C27438" H 7400 3000 50  0001 C CNN "DistOrderNr"
	1    7400 3000
	1    0    0    -1  
$EndComp
$Comp
L htl_virtual:Item Item?
U 1 1 60EAC806
P 7400 3300
AR Path="/60EAC806" Ref="Item?"  Part="1" 
AR Path="/60CB434E/60EAC806" Ref="Item7"  Part="1" 
F 0 "Item7" H 7530 3346 50  0000 L CNN
F 1 "Header 8x2.54" H 7530 3255 50  0000 L CNN
F 2 "htl_virtual:Virtual-Item" H 7400 3230 50  0001 C CNN
F 3 "~" V 7400 3300 50  0001 C CNN
F 4 "LCSC" H 7400 3300 50  0001 C CNN "Distributor"
F 5 "C27438" H 7400 3300 50  0001 C CNN "DistOrderNr"
	1    7400 3300
	1    0    0    -1  
$EndComp
$Comp
L htl_virtual:Item Item?
U 1 1 60EAC80D
P 7450 5350
AR Path="/60EAC80D" Ref="Item?"  Part="1" 
AR Path="/60CB434E/60EAC80D" Ref="Item8"  Part="1" 
F 0 "Item8" H 7580 5396 50  0000 L CNN
F 1 "Header 8x2.54" H 7580 5305 50  0000 L CNN
F 2 "htl_virtual:Virtual-Item" H 7450 5280 50  0001 C CNN
F 3 "~" V 7450 5350 50  0001 C CNN
F 4 "LCSC" H 7450 5350 50  0001 C CNN "Distributor"
F 5 "C27438" H 7450 5350 50  0001 C CNN "DistOrderNr"
	1    7450 5350
	1    0    0    -1  
$EndComp
$Comp
L htl_virtual:Item Item?
U 1 1 60EAC814
P 7450 5550
AR Path="/60EAC814" Ref="Item?"  Part="1" 
AR Path="/60CB434E/60EAC814" Ref="Item9"  Part="1" 
F 0 "Item9" H 7580 5596 50  0000 L CNN
F 1 "Header 8x2.54" H 7580 5505 50  0000 L CNN
F 2 "htl_virtual:Virtual-Item" H 7450 5480 50  0001 C CNN
F 3 "~" V 7450 5550 50  0001 C CNN
F 4 "LCSC" H 7450 5550 50  0001 C CNN "Distributor"
F 5 "C27438" H 7450 5550 50  0001 C CNN "DistOrderNr"
	1    7450 5550
	1    0    0    -1  
$EndComp
$Comp
L Motor:Stepper_Motor_bipolar M?
U 1 1 60F044AE
P 5950 3400
F 0 "M?" H 6138 3524 50  0000 L CNN
F 1 "17HS15-1504S-X1" H 6138 3433 50  0000 L CNN
F 2 "" H 5960 3390 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 5960 3390 50  0001 C CNN
	1    5950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3250 5350 3250
Wire Wire Line
	5650 3250 5650 3300
Wire Wire Line
	5650 3500 5550 3500
Wire Wire Line
	5350 3500 5350 3450
Wire Wire Line
	5350 3450 5250 3450
Wire Wire Line
	5250 3050 5350 3050
Wire Wire Line
	5850 3050 5850 3100
Wire Wire Line
	6050 3100 6050 2850
Wire Wire Line
	6050 2850 5550 2850
Wire Wire Line
	5350 2850 5250 2850
Wire Wire Line
	5550 3050 5850 3050
Wire Wire Line
	5550 3250 5650 3250
$Comp
L t14_testing:Wire W?
U 1 1 60F0E544
P 5450 3500
F 0 "W?" H 5450 3690 50  0001 C CNN
F 1 "blau" H 5450 3599 50  0000 C CNN
F 2 "" H 5450 3500 50  0001 C CNN
F 3 "" H 5450 3500 50  0001 C CNN
	1    5450 3500
	1    0    0    -1  
$EndComp
$Comp
L t14_testing:Wire W?
U 1 1 60F0E809
P 5450 3250
F 0 "W?" H 5450 3440 50  0001 C CNN
F 1 "rot" H 5450 3349 50  0000 C CNN
F 2 "" H 5450 3250 50  0001 C CNN
F 3 "" H 5450 3250 50  0001 C CNN
	1    5450 3250
	1    0    0    -1  
$EndComp
$Comp
L t14_testing:Wire W?
U 1 1 60F0EA30
P 5450 3050
F 0 "W?" H 5450 3240 50  0001 C CNN
F 1 "schwarz" H 5450 3149 50  0000 C CNN
F 2 "" H 5450 3050 50  0001 C CNN
F 3 "" H 5450 3050 50  0001 C CNN
	1    5450 3050
	1    0    0    -1  
$EndComp
$Comp
L t14_testing:Wire W?
U 1 1 60F0EC52
P 5450 2850
F 0 "W?" H 5450 3040 50  0001 C CNN
F 1 "grün" H 5450 2949 50  0000 C CNN
F 2 "" H 5450 2850 50  0001 C CNN
F 3 "" H 5450 2850 50  0001 C CNN
	1    5450 2850
	1    0    0    -1  
$EndComp
$Comp
L Motor:Stepper_Motor_bipolar M?
U 1 1 60F1622F
P 6000 5700
F 0 "M?" H 6188 5824 50  0000 L CNN
F 1 "17HS15-1504S-X1" H 6188 5733 50  0000 L CNN
F 2 "" H 6010 5690 50  0001 C CNN
F 3 "http://www.infineon.com/dgdl/Application-Note-TLE8110EE_driving_UniPolarStepperMotor_V1.1.pdf?fileId=db3a30431be39b97011be5d0aa0a00b0" H 6010 5690 50  0001 C CNN
	1    6000 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 5550 5400 5550
Wire Wire Line
	5700 5550 5700 5600
Wire Wire Line
	5700 5800 5600 5800
Wire Wire Line
	5400 5800 5400 5750
Wire Wire Line
	5400 5750 5300 5750
Wire Wire Line
	5300 5350 5400 5350
Wire Wire Line
	5900 5350 5900 5400
Wire Wire Line
	6100 5400 6100 5150
Wire Wire Line
	6100 5150 5600 5150
Wire Wire Line
	5400 5150 5300 5150
Wire Wire Line
	5600 5350 5900 5350
Wire Wire Line
	5600 5550 5700 5550
$Comp
L t14_testing:Wire W?
U 1 1 60F16241
P 5500 5800
F 0 "W?" H 5500 5990 50  0001 C CNN
F 1 "blau" H 5500 5899 50  0000 C CNN
F 2 "" H 5500 5800 50  0001 C CNN
F 3 "" H 5500 5800 50  0001 C CNN
	1    5500 5800
	1    0    0    -1  
$EndComp
$Comp
L t14_testing:Wire W?
U 1 1 60F16247
P 5500 5550
F 0 "W?" H 5500 5740 50  0001 C CNN
F 1 "rot" H 5500 5649 50  0000 C CNN
F 2 "" H 5500 5550 50  0001 C CNN
F 3 "" H 5500 5550 50  0001 C CNN
	1    5500 5550
	1    0    0    -1  
$EndComp
$Comp
L t14_testing:Wire W?
U 1 1 60F1624D
P 5500 5350
F 0 "W?" H 5500 5540 50  0001 C CNN
F 1 "schwarz" H 5500 5449 50  0000 C CNN
F 2 "" H 5500 5350 50  0001 C CNN
F 3 "" H 5500 5350 50  0001 C CNN
	1    5500 5350
	1    0    0    -1  
$EndComp
$Comp
L t14_testing:Wire W?
U 1 1 60F16253
P 5500 5150
F 0 "W?" H 5500 5340 50  0001 C CNN
F 1 "grün" H 5500 5249 50  0000 C CNN
F 2 "" H 5500 5150 50  0001 C CNN
F 3 "" H 5500 5150 50  0001 C CNN
	1    5500 5150
	1    0    0    -1  
$EndComp
NoConn ~ 5250 2850
NoConn ~ 5250 3050
NoConn ~ 5250 3250
NoConn ~ 5250 3450
NoConn ~ 5300 5150
NoConn ~ 5300 5350
NoConn ~ 5300 5550
NoConn ~ 5300 5750
$EndSCHEMATC
